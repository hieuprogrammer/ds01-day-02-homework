numbers = range(0, 99999)


def print_even_numbers():
    print('Even numbers in range 0 to 99999:')
    for a in numbers:
        if a % 2 == 0:
            print(str(a) + ' ', end='')
    print('\n')


def print_odd_numbers():
    print('Odd numbers in range 0 to 99999:')
    for a in numbers:
        if a % 2 != 0:
            print(str(a) + ' ', end='')
    print('\n')


print_even_numbers()
print_odd_numbers()
