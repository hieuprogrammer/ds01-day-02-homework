import csv


def tinh_tan_so_va_tan_suat_theo_dia_diem():
    with open('data-sources/user-ratings.csv', 'r', encoding='utf8') as file:
        count_tphcm = 0
        count_hn = 0
        count_dn = 0

        data = csv.reader(file, delimiter=',')
        for row in data:
            for element in row:
                if element == 'TP HCM':
                    count_tphcm += 1
                if element == 'Hà Nội':
                    count_hn += 1
                if element == 'Đà Nẵng':
                    count_dn += 1

        print('------------------------------ TẦN SỐ ------------------------------')
        print(f'Tần số khách hàng mua ở TP.HCM là: {str(count_tphcm)}.')
        print(f'Tần số khách hàng mua ở Hà Nội là: {str(count_hn)}.')
        print(f'Tần số khách hàng mua ở Đà Nẵng là: {str(count_dn)}.\n')

        print('------------------------------ TẦN SUẤT ------------------------------')
        print(
            f'Tần suất khách hàng mua ở TP.HCM là: {str((count_tphcm / (count_tphcm + count_hn + count_dn)) * 100)}%.')
        print(f'Tần suất khách hàng mua ở Hà Nội là: {str((count_hn / (count_tphcm + count_hn + count_dn)) * 100)}%.')
        print(f'Tần suất khách hàng mua ở Đà Nẵng là: {str((count_dn / (count_tphcm + count_hn + count_dn)) * 100)}%.')
    print('\n')


def tinh_tan_so_va_tan_suat_theo_gioi_tinh():
    with open('data-sources/user-ratings.csv', 'r', encoding='utf8') as file:
        count_male = 0
        count_female = 0

        data = csv.reader(file, delimiter=',')
        for row in data:
            for element in row:
                if element == 'Nam':
                    count_male += 1
                if element == 'Nữ':
                    count_female += 1

        print('------------------------------ TẦN SỐ ------------------------------')
        print(f'Tần số khách hàng mua là người nam: {str(count_male)}.')
        print(f'Tần số khách hàng mua là người nữ: {str(count_female)}.\n')

        print('------------------------------ TẦN SUẤT ------------------------------')
        print(f'Tần suất khách hàng mua là người nam: {str((count_male / (count_male + count_female)) * 100)}%.')
        print(f'Tần suất khách hàng mua là người nữ: {str((count_female / (count_male + count_female)) * 100)}%.')
    print('\n')


def tinh_mean_tong_don():
    with open('data-sources/user-ratings.csv', 'r', encoding='utf8') as file:
        count_tong_don = 0
        tong_don_sum = 0.0

        data = list(csv.reader(file, delimiter=','))
        for row in data[1:]:
            count_tong_don += 1
            tong_don_sum += float(row[2])

        tong_don_mean = tong_don_sum / count_tong_don
        print(f'Mean của "Tổng Đơn" là: {str(tong_don_mean)}')


def tinh_mean_ratings():
    with open('data-sources/user-ratings.csv', 'r', encoding='utf8') as file:
        count_ratings = 0
        ratings_sum = 0.0

        data = list(csv.reader(file, delimiter=','))
        for row in data[1:]:
            count_ratings += 1
            ratings_sum += float(row[3])

        ratings_mean = ratings_sum / count_ratings
        print(f'Mean của "Ratings" là: {str(ratings_mean)}')


tinh_tan_so_va_tan_suat_theo_dia_diem()
tinh_tan_so_va_tan_suat_theo_gioi_tinh()
tinh_mean_tong_don()
tinh_mean_ratings()
