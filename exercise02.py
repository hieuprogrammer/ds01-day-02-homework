import exercise01

exercise01.print_even_numbers()
exercise01.print_odd_numbers()


def write_data_to_file():
    data = open("data-sources/data.txt", "w+")
    for a in exercise01.numbers:
        data.write(str(a) + ' ')
    data.close()


write_data_to_file()
